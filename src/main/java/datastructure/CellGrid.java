package datastructure;

import java.lang.reflect.Array;

import cellular.CellState;

public class CellGrid implements IGrid {

    /** The number of rows */
    private int rows;

    /** The number of columns */
    private int columns;

    /** The initial cell state */
    private CellState initialState;

    private CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.columns = columns;
        this.initialState = initialState;
        grid = new CellState[rows][columns];
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (row < 0 || row >= numRows()) {
            throw new IndexOutOfBoundsException();
        } else if (column < 0 || column >= numColumns()) {
            throw new IndexOutOfBoundsException();
        } else {
            this.grid[row][column] = element;
        }
    }

    @Override
    public CellState get(int row, int column) {
        if (row < 0 || row-1 >= numRows()) {
            throw new IndexOutOfBoundsException();
        } else if (column < 0 || column-1 >= numColumns()) {
            throw new IndexOutOfBoundsException();
        } else {
            // get the cellstate in grid
            return this.grid[row][column];
        }/*
        if(((row < numRows() && row >= 0) && (column < numColumns() && column >= 0))){
            return this.grid[row][column];
        }
        else{
            throw new IndexOutOfBoundsException("Index out of bounds: row = " + row + " col = " + column + "," + numRows() + numColumns());
        }*/

    }

    @Override
    public IGrid copy() {
        IGrid copyGrid = new CellGrid(numRows(), numColumns(), initialState);
        for (int i = 0; i < numRows(); i++) {
            for (int j = 0; j < numColumns(); j++) {
                copyGrid.set(i, j, get(i, j));
            }
        }
        return copyGrid;
    }
    
}
